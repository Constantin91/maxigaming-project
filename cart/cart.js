const cart = JSON.parse(localStorage.getItem("cart"));
cart.forEach(cart => {
    $.ajax({
        url: '../flux/product.php',
        type: 'GET',
        dataType: 'json',
        data: {
            choice: 'select_id',
            id : cart.id_product
        },
        success: (res) => {
            console.log(res);
            if(res.success){
                const tr = $("<tr></tr>").attr("id", "tr_" + cart.id_product);
                const name = $("<th></th>").text(res.product.name_product);
                const picturs = $("<th></th>").text(res.product.picture);
                const quantity = $("<th></th>").text(cart.quantity);
                const price = $("<th></th>").text(cart.price_product);
                const id_category = $("<th></th>").text(cart.id_category);


                quantity.attr("id", "qty_" + cart.id_product);

                const del = $("<td></td>");
                const delButton = $("<button></button>").text("Supprimer");
                delButton.attr("id", "del_" + cart.id_product);

                del.append(delButton);
                tr.append(name,picturs,quantity, price,id_category, del,);
                $("table").append(tr); 

                $('#del_' + cart.id_product).click(()=>{
                    removeCart(cart.id_product);
                })
            }
        }
    })
});

function removeCart(id_product){
    let basket = JSON.parse(localStorage.getItem("cart"));
    const i = basket.findIndex(cart => cart.id_product == id_product);

    basket[i].quantity--;
    $("#qty_" + id_product).text(basket[i].quantity);
    if(basket[i].quantity <= 0){
        $("#tr_" + id_product).remove();
        basket.splice(i, 1);
    }
    localStorage.setItem("cart", JSON.stringify(basket));
}

// function totalPrice(id_product) {
//     let basket = getBasket();
//     let total = 0;
//     for (let product of basket) {
//         number += product.quantity;
//         return number; 
//     }
// }

// function getBasket(id_product) {
//     let basket = localStorage.getItem("basket");
//     if (basket == nul) {
//     } else {
//         return JSON.parse(basket);
//     }
// }
