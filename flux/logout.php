<?php
session_start();

$_SESSION['connected'] = false;
unset($_SESSION['id']);
unset($_SESSION['admin']);

json_encode(['success' => true]);
