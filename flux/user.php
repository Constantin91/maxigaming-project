<?php
session_start();
require_once("../utils/db_connect.php");

if (!$_SESSION['connected']) {
    echo json_encode(["success" => false, "error" => "Vous n'êtes pas connecté"]);
    die;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') $method = $_POST;
else $method = $_GET;

switch ($method['choice']) {
    case 'select':
        $req = $db->query("SELECT id_user, CONCAT(firstname, ' ', lastname) AS fullname, birthdate, email FROM users");

        while ($user = $req->fetch(PDO::FETCH_ASSOC)) $users[] = $user;

        echo json_encode(["success" => true, "users" => $users]);
        break;

    case "select_id":
        if (isset($method['id_user']) && !empty(trim($method['id_user']))) {
            $req = $db->prepare("SELECT id, CONCAT(firstname, ' ', lastname) AS fullname, birthdate, email FROM users WHERE id_user = ?");
            $req->execute([$method['id_user']]);

            $user = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "user" => $user]);
        } else echo json_encode(["success" => false, "error" => "Identifiant de l'utilisateur non renseigné"]);
        break;

    case 'update':
        if (
            isset($method['firstname'], $method['lastname'], $method['birthdate'], $method['email'], $method['id_user']) &&
            !empty(trim($method['firstname'])) &&
            !empty(trim($method['lastname'])) &&
            !empty(trim($method['birthdate'] || empty(trim($method['birthdate'])))) &&
            !empty(trim($method['email'])) &&
            !empty(trim($method['id_user']))
        ) {
            $sql = "UPDATE users SET firstname = :firstname, lastname = :lastname, birthdate = :birthdate, email = :email WHERE id_user = :id_user ";
            $req = $db->prepare($sql);
            $req->bindValue(':firstname', $method['firstname']);
            $req->bindValue(':lastname', $method['lastname']);
            $req->bindValue(':birthdate', $method['birthdate']);
            $req->bindValue(':email', $method['email']);
            $req->bindValue(':id_user', $method['id_user']);
            $req->execute();

            echo json_encode(["success" => true]);
        } else echo json_encode(["success" => false, "error" => "Les données ne sont pas correctement renseignée"]);
        break;

    default:
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}//*
