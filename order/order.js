const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('com');
$.ajax({
    url: '../flux/order.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select',
        id
    },
    success: (res) => {
        // console.log(res);
        if (res.success) {
            // console.log(res.orders);
            res.orders.forEach(order => {
                console.log(order);
                const tr = $('<tr></tr>');

                tr.attr('id', 'tr_' + order.id)

                
                const firstname = $('<td></td>').text(order.firstname);
                const lastname = $('<td></td>').text(order.lastname);
                const  address = $('<td></td>').text(order.address);
                const city = $('<td></td>').text(order.city);
                const zip_code = $('<td></td>').text(order.zip_code);
                const phone = $('<td></td>').text(order.phone);
                const price_com = $('<td></td>').text(order.price_com);
                const num_com = $('<td></td>').text(order.num_com);

                tr.append(firstname, lastname, address, city, zip_code, phone, price_com, num_com);
                $('table').append(tr);
            })
        }
    }
})