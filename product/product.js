const urlParams = new URLSearchParams(window.location.search);
const id_category = urlParams.get('cat') || 1;

$.ajax({
    url: '../flux/product.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select_category',
        id_category
    },
    success: (res) => {
        if (res.success) {
            res.products.forEach(product => {
                const card = $('<div></<div>');
                card.attr('class', 'card');

                const imgbox = $('<div></<div>');
                imgbox.attr('class', 'imgbox');

                const content = $('<div></<div>');
                content.attr('class', 'content');

                const price = $('<div></<div>');
                price.attr('class', 'price');

                const button = $('<button></button>');
                button.attr('id', 'ajouter_' + product.id_product);
                button.text('ajouter au panier');

                const name = $('<h2></h2>').text(product.name_product);
                const prix = $('<h3></h3>').text(product.price_product);
                const images = $('<img>').attr('src', product.picture);
                images.attr('id', 'img_' + product.id_category);

                imgbox.append(images, name);
                price.append(prix);
                content.append(price, button);
                card.append(imgbox, content);

                $('#container').append(card);

                $('#ajouter_' + product.id_product).click(() => {
                    addCart(product.id_product);
                })
            });
        } else alert(res.error);
    }
});