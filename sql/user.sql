CREATE TABLE users (
  id_user INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  firstname VARCHAR(45) NOT NULL,
  lastname VARCHAR(45) NOT NULL,
  birthdate DATE NOT NULL,
  email VARCHAR(255) NOT NULL,
  pwd VARCHAR(70) NOT NULL,
  admin TINYINT(1) NOT NULL DEFAULT 0
);

CREATE TABLE categories (
  id_category INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(50) NOT NULL
);

INSERT INTO categories (id_category, name) VALUES (1, 'Jeux videos'), (2, 'Consoles');

CREATE TABLE products (
  id_product INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_product VARCHAR(45) NOT NULL,
  price_product DOUBLE(5,2) NOT NULL,
  picture VARCHAR(255) NOT NULL,
  id_category INT(11) NOT NULL,
  CONSTRAINT fk_id_category FOREIGN KEY (id_category) REFERENCES categories(id_category)
);

CREATE TABLE orders (
  id_order INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_street VARCHAR(55) NOT NULL,
  number_street INT(3) NOT NULL,
  city VARCHAR(55) NOT NULL,
  zip_code INT(5) NOT NULL,
  phone INT(10) NOT NULL,
  price_order DOUBLE(5,2) NOT NULL,
  num_order INT(11) NOT NULL,
  date_order DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  id_user INT(11) NOT NULL, 
  CONSTRAINT fk_id_user FOREIGN KEY (id_user) REFERENCES users(id_user)
);

CREATE TABLE orders_products (
  id_product INT(11) NOT NULL,
  id_order INT(11) NOT NULL,
  quantity INT(250) NOT NULL,
  CONSTRAINT fk_id_product FOREIGN KEY (id_product) REFERENCES products(id_product),
  CONSTRAINT fk_id_order FOREIGN KEY (id_order) REFERENCES orders(id_order),
  PRIMARY KEY(id_product, id_order)
);
