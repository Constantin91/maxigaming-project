function addOrderRow(order) {
    const tr = $('<tr></tr>');

    const num = $('<td></td>');

    const details = $('<a></a>');
    details.text(order.num_order);
    details.attr("href", "order-details/order-details.html?id=" + order.id_order);
    num.append(details);

    const date = $('<td></td>').text(order.date_order);
    const price = $('<td></td>').text(order.price_order);
    const user = $('<td></td>').text(order.fullname);

    tr.append(num, date, price, user);
    $('table').append(tr);
}

$.ajax({
    url: '../../flux/order.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select'
    },
    success: (res) => {
        if (res.success) {
            res.orders.forEach(o => {
                addOrderRow(o)
            });
        } else alert(res.error);
    }
});