let categories = null;
let idToUpdate = null;

function addProductRow(product) {
    const tr = $('<tr></tr>');
    tr.attr('id', 'tr_' + product.id_product);

    const name = $('<td></td>').text(product.name_product);
    name.attr('id', 'name_' + product.id_product);

    let category;
    if (isNaN(product.category)) category = $('<td></td>').text(product.category);
    else category = $('<td></td>').text(categories.find(cat => cat.id_category == product.category).name);
    category.attr('id', 'category_' + product.id_product);

    const price = $('<td></td>').text(product.price_product);
    price.attr('id', 'price_' + product.id_product);

    const imgtd = $('<td></td>');
    imgtd.attr('id', 'img_' + product.id_product);
    const img = $('<img>').attr("src", "../" + product.picture);
    imgtd.append(img);

    const updatebtn = $('<button></button>').text('Modifier');
    updatebtn.attr('id', 'update_' + product.id_product);
    const updatetd = $('<td></td>').append(updatebtn);

    const deletbtn = $('<button></button>').text('Supprimer');
    deletbtn.attr('id', 'delete_' + product.id_product);
    const deletetd = $('<td></td>').append(deletbtn);

    tr.append(name, category, price, imgtd, updatetd, deletetd);
    $('table').append(tr);

    $('#update_' + product.id_product).click(() => {
        getProduct(product.id_product);
    });

    $('#delete_' + product.id_product).click(() => {
        if (confirm("Etes vous sur de vouloir supprimer ce produit ?")) {
            deleteProduct(product.id_product);
        }
    });
}

$.ajax({
    url: '../../flux/product.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select'
    },
    success: (res) => {
        if (res.success) {
            res.products.forEach(p => {
                addProductRow(p)
            });
        } else alert(res.error);
    }
});


$('#submit').click((e) => {
    e.preventDefault();

    idToUpdate ? updateProduct() : createProduct();
});

function getProduct(id) {
    $.ajax({
        url: '../../flux/product.php',
        type: 'GET',
        dataType: 'json',
        data: {
            choice: 'select_id',
            id
        },
        success: (res) => {
            if (res.success) {
                $('#category').val(res.product.id_category);
                $('#name_product').val(res.product.name_product);
                $('#price_product').val(res.product.price_product);
                idToUpdate = res.product.id_product;
            } else alert(res.error);
        }
    });
}

function createProduct() {
    const files = $('#file')[0].files;

    if (files.length > 0) {
        const fd = new FormData();
        fd.append('file', files[0]);

        $.ajax({
            url: "../../flux/admin/upload.php",
            type: "POST",
            dataType: "json",
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            success: (upload) => {
                if (upload.success) {
                    $.ajax({
                        url: '../../flux/admin/product.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            choice: "insert",
                            id_category: $("#category").val(),
                            name_product: $("#name_product").val(),
                            price_product: $("#price_product").val(),
                            picture: upload.picture
                        },
                        success: (res) => {
                            res.success ? addProductRow({
                                id_product: res.id_product,
                                name_product: $("#name_product").val(),
                                price_product: $("#price_product").val(),
                                category: $("#category").val(),
                                picture: upload.picture
                            }) : alert(res.error);

                            document.getElementById("form").reset();
                        }
                    });
                } else alert(upload.error);
            }
        });
    } else alert("Aucune image sélectionnée !");
}

function updateProduct() {
    const files = $('#file')[0].files;

    if (files.length > 0) {
        const fd = new FormData();
        fd.append('file', files[0]);

        $.ajax({
            url: "../../flux/admin/upload.php",
            type: "POST",
            dataType: "json",
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            success: (upload) => {
                if (upload.success) {
                    $.ajax({
                        url: '../../flux/admin/product.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            choice: 'update',
                            id_product: idToUpdate,
                            name_product: $("#name_product").val(),
                            price_product: $("#price_product").val(),
                            id_category: $("#category").val(),
                            picture: upload.picture
                        },
                        success: (res) => {
                            if (res.success) {
                                $("#name_" + idToUpdate).text($("#name_product").val());
                                $("#price_" + idToUpdate).text($("#price_product").val());
                                $("#category_" + idToUpdate).text(categories.find(cat => cat.id_category == $("#category").val()).name);
                                $("#img_" + idToUpdate + " img").attr("src", "../" + upload.picture);
                            } else alert(res.error);

                            idToUpdate = null;
                            document.getElementById("form").reset();
                        }
                    });
                } else alert(upload.error);
            }
        });
    } else alert("Aucune image sélectionnée !");
}

function deleteProduct(id) {
    $.ajax({
        url: '../../flux/admin/product.php',
        type: 'GET',
        dataType: 'json',
        data: {
            choice: 'delete',
            id
        },
        success: (res) => {
            res.success ? $('#tr_' + id).remove() : alert(res.error);
        }
    });
}

$.ajax({
    url: '../../flux/category.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select'
    },
    success: (res) => {
        if (res.success) {
            categories = res.categories;
            res.categories.forEach(category => {
                const option = $("<option></option>");
                option.val(category.id_category);
                option.text(category.name);

                $("#category").append(option);
            })
        } else alert(res.error)
    }
});