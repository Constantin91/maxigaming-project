<?php
session_start();
require_once("../utils/db_connect.php");

if (!$_SESSION['connected']) {
    echo json_encode(["success" => false, "error" => "Vous n'êtes pas connecté"]);
    die;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') $method = $_POST;
else $method = $_GET;


switch ($method['choice']) {
    case 'select':
        $req = $db->query("SELECT o.*, CONCAT(u.firstname, ' ', u.lastname) AS fullname FROM orders o INNER JOIN users u ON u.id_user = o.id_user");

        while ($order = $req->fetch(PDO::FETCH_ASSOC)) $orders[] = $order;

        echo json_encode(["success" => true, "orders" => $orders]);
        break;

    case "select_id":
        if (isset($method['id']) && !empty(trim($method['id']))) {
            $req = $db->prepare("SELECT o.*, CONCAT(u.firstname, ' ', u.lastname) AS fullname FROM orders o INNER JOIN users u ON u.id_user = o.id_user WHERE o.id_order = ?");
            $req->execute([$method['id']]);
            $order = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "order" => $order]);
        } else echo json_encode(["success" => false, "error" => "Identifiant de la commande non renseigné"]);
        break;

    case "order_product":
        if (isset($method['id']) && !empty(trim($method['id']))) {
            $req = $db->prepare("SELECT p.*, op.quantity, c.name FROM products p INNER JOIN orders_products op ON op.id_product = p.id_product INNER JOIN categories c ON c.id_category = p.id_category WHERE op.id_order = ?");
            $req->execute([$method['id']]);

            while ($order_product = $req->fetch(PDO::FETCH_ASSOC)) $order_products[] = $order_product;

            echo json_encode(["success" => true, "order_products" => $order_products]);
        } else echo json_encode(["success" => false, "error" => "Identifiant de la commande non renseigné"]);
        break;

    default:
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
