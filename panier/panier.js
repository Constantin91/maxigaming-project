function addCart(id_product) { 

    let cart = JSON.parse(localStorage.getItem("cart"))

    if(!cart){

        cart = [{id_product, quantity:1}]
    } else {
        const i = cart.findIndex(cart => cart.id_product == id_product);
        if(i >= 0){
            cart[i].quantity++;
        } else {
            cart.push({id_product, quantity:1});
        }
    }
    localStorage.setItem("cart", JSON.stringify(cart));
 }      