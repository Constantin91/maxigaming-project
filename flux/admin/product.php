<?php
session_start();
require_once("../../utils/db_connect.php");

// if (!$_SESSION['connected']) {
//     echo json_encode(["success" => false, "error" => "Vous n'êtes pas connecté"]);
//     die;
// }

if (!$_SESSION['admin']) {
    echo json_encode(["success" => false, "error" => "Vous n'êtes pas administrateur, accès interdit"]);
    die;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') $method = $_POST;
else $method = $_GET;

switch ($method['choice']) {
    case 'delete':
        if (isset($method['id']) && !empty(trim($method['id']))) {
            $req = $db->prepare("DELETE FROM products WHERE id_product = ?");
            $req->execute([$method['id']]);

            echo json_encode(["success" => true]);
        } else echo json_encode(["success" => false, "error" => "Identifiant non renseigné, suppression impossible"]);
        break;

    case 'update':
        if (
            isset($method['name_product'], $method['price_product'], $method['picture'], $method['id_product'], $method['id_category'],) &&
            !empty(trim($method['name_product'])) &&
            !empty(trim($method['price_product'])) &&
            !empty(trim($method['picture'])) &&
            !empty(trim($method['id_category'])) &&
            !empty(trim($method['id_product']))
        ) {
            $sql = "UPDATE products SET name_product = :name_product, price_product = :price_product, picture = :picture, id_category = :id_category WHERE id_product = :id_product";
            $req = $db->prepare($sql);
            $req->bindValue(':name_product', $method['name_product']);
            $req->bindValue(':price_product', $method['price_product']);
            $req->bindValue(':picture', $method['picture']);
            $req->bindValue(':id_product', $method['id_product']);
            $req->bindValue(':id_category', $method['id_category']);
            $req->execute();

            echo json_encode(["success" => true]);
        } else echo json_encode(["success" => false, "error" => "Les données ne sont pas correctement renseignée"]);
        break;

    case 'insert':
        if (
            isset($method['name_product'], $method['price_product'], $method['id_category'], $method['picture']) &&
            !empty(trim($method['name_product'])) &&
            !empty(trim($method['price_product'])) &&
            !empty(trim($method['id_category'])) &&
            !empty(trim($method['picture']))
        ) {
            $sql = "INSERT INTO products (name_product, price_product, picture, id_category) VALUES (:name_product, :price_product, :picture, :id_category)";
            $req = $db->prepare($sql);
            $req->bindValue(':name_product', $method['name_product']);
            $req->bindValue(':price_product', $method['price_product']);
            $req->bindValue(':picture', $method['picture']);
            $req->bindValue(':id_category', $method['id_category']);
            $req->execute();

            echo json_encode(["success" => true, 'id_product' => $db->lastInsertId()]);
        } else echo json_encode(["success" => false, "error" => "Les données ne sont pas correctement renseignée"]);
        break;

    default:
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
