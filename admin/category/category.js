let idToUpdate = null;

function addCategoryRow(category) {
    const tr = $('<tr></tr>');
    tr.attr('id', 'tr_' + category.id_category);
    
    const name = $('<td></td>').text(category.name);
    name.attr('id', 'name_' + category.id_category);

    const updatebtn = $('<button></button>').text('Modifier');
    updatebtn.attr('id', 'update_' + category.id_category);
    const updatetd = $('<td></td>').append(updatebtn);

    const deletbtn = $('<button></button>').text('Supprimer');
    deletbtn.attr('id', 'delete_' + category.id_category);
    const deletetd = $('<td></td>').append(deletbtn);

    tr.append(name, updatetd, deletetd);
    $('table').append(tr);

    $('#update_' + category.id_category).click(() => {
        getCategory(category.id_category);
    });

    $('#delete_' + category.id_category).click(() => {
        if (confirm("Etes vous sur de vouloir supprimer ce produit ?")) {
            deleteCategory(category.id_category);
        }
    });
}

$.ajax({
    url: '../../flux/category.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select'
    },
    success: (res) => {
        if (res.success) {
            res.categories.forEach(cat => { addCategoryRow(cat) });
        } else alert(res.error);
    }
});


$('#submit').click((e) => {
    e.preventDefault();

    idToUpdate ? updateCategory() : createCategory();
});

function getCategory(id) {
    $.ajax({
        url: '../../flux/category.php',
        type: 'GET',
        dataType: 'json',
        data: {
            choice: 'select_id',
            id
        },
        success: (res) => {
            if (res.success) {
                $('#category').val(res.category.name);
                idToUpdate = res.category.id_category;
            } else alert(res.error);
        }
    });
}

function createCategory() {
    $.ajax({
        url: '../../flux/admin/category.php',
        type: 'POST',
        dataType: 'json',
        data: {
            choice: 'insert',
            name: $("#category").val()
        },
        success: (res) => {
            res.success ? addCategoryRow({ id_category: res.id_category, name: $("#category").val()}) : alert(res.error);
            document.getElementById("form").reset();
        }
    });
}

function updateCategory() {
    $.ajax({
        url: '../../flux/admin/category.php',
        type: 'POST',
        dataType: 'json',
        data: {
            choice: 'update',
            id_category: idToUpdate,
            name: $("#category").val()
        },
        success: (res) => {
            res.success ? $("#name_" + idToUpdate).text($("#category").val()) : alert(res.error);

            idToUpdate = null;
            document.getElementById("form").reset();
        }
    });
}

function deleteCategory(id) {
    $.ajax({
        url: '../../flux/admin/category.php',
        type: 'GET',
        dataType: 'json',
        data: {
            choice: 'delete',
            id
        },
        success: (res) => {
            res.success ? $('#tr_' + id).remove() : alert(res.error);
        }
    });
}