function addUserRow(user) {
    const tr = $('<tr></tr>');

    const name = $('<td></td>').text(user.fullname);
    const birthdate = $('<td></td>').text(user.birthdate);
    const email = $('<td></td>').text(user.email);

    tr.append(name, birthdate, email);
    $('table').append(tr);
}

$.ajax({
    url: '../../flux/user.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select'
    },
    success: (res) => {
        if (res.success) {
            res.users.forEach(u => {
                addUserRow(u)
            });
        } else alert(res.error);
    }
});




