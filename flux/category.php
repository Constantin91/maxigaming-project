<?php
session_start();
require_once("../utils/db_connect.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') $method = $_POST;
else $method = $_GET;


switch ($method['choice']) {
    case 'select':
        $req = $db->query("SELECT * FROM categories");

        while ($categorie = $req->fetch(PDO::FETCH_ASSOC)) $categories[] = $categorie;

        echo json_encode(["success" => true, "categories" => $categories]);
        break;


    case "select_id":
        if (isset($method['id']) && !empty(trim($method['id']))) {
            $req = $db->prepare("SELECT * FROM categories WHERE id_category = ?");
            $req->execute([$method['id']]);
            $category = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "category" => $category]);
        } else echo json_encode(["success" => false, "error" => "Identifiant de la catégorie non renseigné"]);
        break;

    default:
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
