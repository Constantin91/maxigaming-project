<?php
session_start();
require_once("../utils/db_connect.php");

// if (!$_SESSION['connected']) {
//     echo json_encode(["success" => false, "error" => "Vous n'êtes pas connecté"]);
//     die;
// }

if ($_SERVER['REQUEST_METHOD'] == 'POST') $method = $_POST;
else $method = $_GET;

switch ($method['choice']) {
    case 'select':
        $req = $db->query("SELECT p.*, c.name AS category FROM products p INNER JOIN categories c ON c.id_category = p.id_category");

        while ($product = $req->fetch(PDO::FETCH_ASSOC)) $products[] = $product;

        echo json_encode(["success" => true, "products" => $products]);
        break;

    case 'select_category':
        if (isset($method['id_category']) && !empty(trim($method['id_category']))) {
            $req = $db->prepare("SELECT * FROM products WHERE id_category = ?");
            $req->execute([$method['id_category']]);

            while ($product = $req->fetch(PDO::FETCH_ASSOC)) $products[] = $product;

            echo json_encode(["success" => true, "products" => $products]);
        } else echo json_encode(["success" => false, "error" => "Identifiant de la catégorie non renseigné"]);
        break;

    case "select_id":
        if (isset($method['id']) && !empty(trim($method['id']))) {
            $req = $db->prepare("SELECT * FROM products WHERE id_product = ? ");
            $req->execute([$method['id']]);
            $products = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "product" => $products]);
        } else echo json_encode(["success" => false, "error" => "Identifiant du produit non renseigné"]);
        break;

    default:
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
