<?php
session_start();
require_once("../../utils/db_connect.php");

if (!$_SESSION['connected']) {
    echo json_encode(["success" => false, "error" => "Vous n'êtes pas connecté"]);
    die;
}

if (!$_SESSION['admin']) {
    echo json_encode(["success" => false, "error" => "Vous n'êtes pas administrateur, accès interdit"]);
    die;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') $method = $_POST;
else $method = $_GET;

switch ($method['choice']) {
    case 'delete':
        if (isset($method['id']) && !empty(trim($method['id']))) {
            $req = $db->prepare("DELETE FROM categories WHERE id_category = ?");
            $req->execute([$method['id']]);

            echo json_encode(["success" => true]);
        } else echo json_encode(["success" => false, "error" => "Identifiant non renseigné, suppression impossible"]);
        break;

    case 'update':
        if (
            isset($method['name'], $method['id_category'],) &&
            !empty(trim($method['name'])) &&
            !empty(trim($method['id_category']))
        ) {
            $req = $db->prepare("UPDATE categories SET name = :name WHERE id_category = :id_category");
            $req->bindValue(':name', $method['name']);
            $req->bindValue(':id_category', $method['id_category']);
            $req->execute();

            echo json_encode(["success" => true]);
        } else echo json_encode(["success" => false, "error" => "Les données ne sont pas correctement renseignée"]);
        break;

    case 'insert':
        if (
            isset($method['name']) && !empty(trim($method['name']))
        ) {
            $req = $db->prepare("INSERT INTO categories (name) VALUES (:name)");
            $req->bindValue(':name', $method['name']);
            $req->execute();

            echo json_encode(["success" => true, 'id_category' => $db->lastInsertId()]);
        } else echo json_encode(["success" => false, "error" => "Les données ne sont pas correctement renseignée"]);
        break;

    default:
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
