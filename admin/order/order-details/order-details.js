const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('id');

$.ajax({
    url: '../../../flux/order.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select_id',
        id
    },
    success: (res) => {
        if (res.success) {
            const num = $('<li></li>').text("Numéro: " + res.order.num_order);
            const date = $('<li></li>').text("Date: " + res.order.date_order);
            const price = $('<li></li>').text("Prix total: " + res.order.price_order);
            const user = $('<li></li>').text("Client: " + res.order.fullname);

            $("#order").append(num, date, price, user);
        } else alert(res.error);
    }
});

$.ajax({
    url: '../../../flux/order.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'order_product',
        id
    },
    success: (res) => {
        if (res.success) {
            res.order_products.forEach(op => {
                const ctn = $('<ul></ul>')

                const name = $('<li></li>').text("Nom produit: " + op.name_product);
                const price = $('<li></li>').text("Prix unitaire: " + op.price_product);
                const qty = $('<li></li>').text("Quantité: " + op.quantity);
                const category = $('<li></li>').text("Catégorie: " + op.name);

                ctn.append(name, price, qty, category);

                $("#order_product").append(ctn);
            });
        } else alert(res.error);
    }
});