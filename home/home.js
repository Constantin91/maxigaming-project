const maintab = ["../image/horizon.jpg", "../image/jeux1.jpg", "../image/jeux2.jpg"];

let change = 0;
function carrousel() {
    do {
        if (change == maintab.length) change = 0;
        change++;
    } while (change == maintab.length);

    $('#contenu img').attr('src', maintab[change]);
}

let last = 0;
$("#contenu img").first().click(() => {
    do {
        change = Math.floor(Math.random() * maintab.length);
    } while (change == last);
    last = change;
    $("#contenu img").first().attr('src', maintab[change]);
});

setInterval(carrousel, 5000);
