$.ajax({
    url: '../flux/category.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select',
    },
    success: (res) => {
        if (res.success) {    
            const home = $('<a></a>');
            home.attr('href', '../home/home.html');
            home.text("MaxiGaming");

            const tabs = $('<div></div>');
            tabs.addClass("onglets");
            res.categories.forEach(categorie => {
                const a = $('<a></a>');
                a.attr('href', '../product/product.html?cat=' + categorie.id_category);
                a.text(categorie.name);

                tabs.append(a);
            });

            const nav = $('<nav></nav>');
            nav.append(home, tabs);
            
            if (localStorage.getItem("connected")) {
                if (localStorage.getItem("admin")) {
                    const admin = $('<a></a>');
                    admin.attr('href', '../admin/category/category.html');
                    admin.text("INTERFACE ADMIN");
                    nav.append(admin);
                }

                // $.ajax({
                //     url: "../login.php",
                //     type: "GET",
                //     dataType: "json",
                //     success: ({admin, connected}) => {
                //       if(admin === false){
                //         $('#admin a').remove();
                //       }
                  
                //       if(connected){
                //         $('#account a').remove();
                //         $('#logout').html('<a href="../logout.php">Déconnexion</a>')
                //       }else{
                //         $('#admin a').remove();
                //         $('.imgcategories a').attr('href','#');
                //       }
                //     }
                //   });

                const profil = $('<a></a>');
                profil.attr('href', '../profil/profil.html');
                profil.text("Profil");

                const cart = $('<a></a>');
                cart.attr('href', '../cart/cart.html');
                cart.text("Panier");

                const logout = $('<a></a>');
                logout.attr('href', '../login/login.html?action=logout');
                logout.text("Déconnexion");

                nav.append(profil,cart, logout);
            } else {
                const login = $('<a></a>');
                login.attr('href', '../login/login.html');
                login.text("Connexion");
                
                nav.append(login);
            }

            $("body").prepend(nav);
        }
    }
});